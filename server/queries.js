const Pool = require('pg').Pool
const pool = new Pool({
  user: 'minhduc',
  host: 'localhost',
  database: 'minhduc_database',
  password: 'duc123321',
  port: 5432,
})

const getStories = (request, response) => {
  pool.query('SELECT * FROM story ORDER BY story_id ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getStoryById = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM story WHERE story_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}
const getMyStories = (request, response) => {
  const author = request.params.author
  console.log('getMyStories'+author)
  pool.query('SELECT * FROM story WHERE story_author = $1', [author], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const createStory = (request, response) => {
    const {story_id, story_title, story_url, story_content, story_created_date,story_image_url,story_author,story_likes,story_comment,story_author_image } = request.body
    pool.query('INSERT INTO public.story(story_id, story_title, story_url, story_content, story_created_date,story_image_url,story_author,story_likes,story_comment,story_author_image) VALUES ($1, $2, $3, $4, $5,$6,$7,$8, $9,$10);', [story_id, story_title, story_url, story_content, story_created_date,story_image_url,story_author,story_likes,story_comment,story_author_image], (error, results) => {
    if (error) {
        console.log(`INSERT INTO public.story(story_id, story_title, story_url, story_content, story_created_date,story_image_url,story_author,story_likes,story_comment ) VALUES (${story_id}, ${story_title}, ${story_url}, ${story_content}, ${story_created_date},${story_image_url},${story_image_url},${story_likes},${story_author_image},ARRAY[${story_comment}])`)
      throw error
          
    }
    response.status(201).json({"message" : `Story added with ID: ${request.body.story_id}`})
  })
} 

const updateStory = (request, response) => { 
  const story_id = parseInt(request.params.id)
  const { story_title, story_content,story_image_url ,story_author_image} = request.body

  pool.query(
    'UPDATE story SET story_title=$1, story_content=$2,story_image_url=$3,story_author_image =$4 WHERE story_id = $5',
    [ story_title, story_content,story_image_url,story_author_image, story_id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json({"message" : `story modified with ID: ${story_id}`})
    }
  )
}

const updateFullStory = (request, response) => {
  const story_id = parseInt(request.params.id)
  const { story_title,story_url, story_content, story_created_date,story_image_url,story_author,story_likes,story_comment } = request.body

  pool.query(
    'UPDATE story SET story_title=$1, story_url=$2, story_content=$3, story_created_date=$4,story_image_url=$5,story_author=$6,story_likes=$7,story_comment=$8 WHERE story_id = $9',
    [ story_title,story_url, story_content, story_created_date,story_image_url,story_author,story_likes,story_comment, story_id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json({"message" : `story modified with ID: ${story_id}`})
    }
  )
}

const deleteStory = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('DELETE FROM public.story WHERE story_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json({"message" : `story deleted with ID: ${id}`})
  })
}

/** 
 * 
 * 
 *                                Users Queries
 * 
 * 
 */

const getUsers = (request, response) => {
  pool.query('SELECT * FROM users ORDER BY user_id ASC', (error, results) => {
    if (error) {
      throw error
    }
    let current_user_name = request.user.name
    pool.query('SELECT * FROM users WHERE user_name = $1',[current_user_name], (error, results2) => {
      if (error) {
        throw error
      }
      let current_user_role = results2.rows[0].user_role
      // let result = results.rows.filter(entity => entity.user_role === current_user_role)
      if(current_user_role == "ADMIN"){
        response.status(200).json(results.rows)
      }else{
        response.status(403).json({
          success: false,
          message: 'you donot have permission'
        });
      }
      
    })
  })
}
const getInfoByHeader = (request, response) => {
  response.status(200).json(request.user)
}

const getUserById = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM users WHERE user_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
} 
const getUserByName = (request, response) => {
  const name = request.params.name

  pool.query('SELECT * FROM users WHERE user_name = $1', [name], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
} 

const checkUserWithUsernameAndPassword = (request, response,next) => {
  // const id = parseInt(request.params.id)
  let user_name = request.body.username;
  let user_password = request.body.password;
  if (user_name && user_password) {
    pool.query('SELECT * FROM users WHERE user_name = $1 AND user_password = $2', [user_name,user_password], (error, results) => {
      if (error) {
        throw error
      }
      // console.log("FROM QUERY")
      // console.log(request.user)
      request.result = results.rows[0]
      next()
    })
  } else {
    res.sendStatus(400).json({
      success: false,
      message: 'Authentication failed! Please check the request'
    });
  }

}

const createUser = (request, response) => {
    const { user_name, user_email, user_password,user_profile_image_url,user_role } = request.body
    pool.query('INSERT INTO public.users( user_name, user_email, user_password,user_profile_image_url,user_role) VALUES ($1, $2, $3,$4,$5);', [user_name, user_email, user_password,user_profile_image_url,user_role], (error, results) => {
    if (error) {
        console.log(`INSERT INTO public.users(user_name, user_email, user_password, user_profile_image_url,user_role) VALUES (${user_name}, ${user_email}, ${user_password}, ${user_profile_image_url}, ${user_role})`)
      throw error
         
    }
    response.status(201).json({"message" : `User added with name: ${request.body.user_name}`})
  })
}


const updateUser = (request, response) => {
  const user_id = parseInt(request.params.id)
  const { user_name, user_email, user_password,user_profile_image_url, user_role } = request.body

  pool.query(
    'UPDATE users SET user_name=$1, user_email=$2, user_password=$3 , user_profile_image_url =$4,user_role = $5 WHERE user_id = $6',
    [ user_name, user_email, user_password,user_profile_image_url, user_role, user_id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json({"message" : `User modified with ID: ${user_id}`})
    }
  )
}

const deleteUser = (request, response) => {
  const user_id = parseInt(request.params.id)

  pool.query('DELETE FROM public.users WHERE user_id = $1', [user_id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json({"message" : `User deleted with ID: ${user_id}`})
  })
}


module.exports = {
  getStories,
  getStoryById,
  getMyStories,
  createStory,
  updateStory,
  updateFullStory,
  deleteStory,

  getUsers,
  getUserById,
  getUserByName,
  createUser,
  updateUser,
  deleteUser,
  checkUserWithUsernameAndPassword,
  getInfoByHeader,
}