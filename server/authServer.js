
require('dotenv').config()

const express = require('express')
const app = express()
const jwt= require('jsonwebtoken')

const bodyParser = require('body-parser')
const db = require('./queries')
const cors = require('cors')

// var corsOptions = {
//   origin: 'http://localhost:3000/',
//   optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204 
// }

// app.use(cors(corsOptions))
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization,responseType');
  next();
});

// dung parse json 
// dung form ?? 
app.use(
  bodyParser.urlencoded({
    extended: true
  })
)
app.use(bodyParser.json())



app.get('/', db.getStories)
app.get('/stories', db.getStories)
app.get('/stories/:id', db.getStoryById)
app.post('/stories', db.createStory)
app.put('/stories/:id', db.updateStory)
app.delete('/stories/:id', db.deleteStory)

app.get('/users', db.getUsers)
app.get('/users/:id', db.getUserById)
app.post('/users', db.createUser)
app.put('/users/:id', db.updateUser)
app.delete('/users/:id', db.deleteUser)

//need to store in local storage
let refreshTokens = []

const posts = [{user_name:'duc',title:'post 1'},{user_name:'ngan',title:'post 2'}]

app.post('/get-another-token', (req,res)=>{
  const refreshToken = req.body.token
  // res.json( {accessToken : refreshToken})
  if (refreshToken == null) return res.sendStatus(401)
  if (!refreshTokens.includes(refreshToken)) return res.sendStatus(403)

  // user o day duoc lay tu refresh token
  jwt.verify(refreshToken,process.env.REFRESH_TOKEN_SECRET,(err,user)=>{
    if(err) return res.sendStatus(403)
    const accessToken = generateAccessToken({name : user.name})
    res.json( {accessToken : accessToken})
  })

  // res.json( {accessToken : refreshToken})
})



app.post('/login', (req,res)=>{
  /** login se sosanh token o env  */

  // authentication user
  const user_name = req.body.user_name
  const user = {name : user_name}
  // create token by sign: +> take our payload and serialize user object
 // in order to seriallize this user object, we need a secret key (in env)
  const accessToken = generateAccessToken(user)

  const refreshToken = jwt.sign(user,process.env.REFRESH_TOKEN_SECRET)

  // push refresh token to the storage 
  refreshTokens.push(refreshToken)

  res.json({accessToken : accessToken, refreshToken:refreshToken})
})

function generateAccessToken(user){
  return jwt.sign(user,process.env.ACCESS_TOKEN_SECRET,{expiresIn:'30s'}) // 30mims

}

// function authenticateToken(req,res,next){
//   const authHeader = req.headers['authorization']
//   // Bearer TOKEN
//   //if we have authheader then return authHeader authen token else : undifined
//   const token =authHeader && authHeader.split(' ')[1]
//   if(token == null) return res.sendStatus(401)

//   jwt.verify(token, process.env.ACCESS_TOKEN_SECRET,(err,user)=>{
//     if(err) return res.sendStatus(403) // token is not valid, you dint have permission 
//     req.user = user // you have permission
//     next()
//   })
// }

const port = process.env.PORT || 8000;
app.listen(8000, () => console.log(`Server running on port ${port}...`))