require('dotenv').config()

console.log(__dirname + '/.env')
const express = require('express')
const app = express()
const jwt = require('jsonwebtoken')

const bodyParser = require('body-parser')
const db = require('./queries')
const cors = require('cors')

//need to store in local storage
let refreshTokens = []
/*************************** Navigator ***************************************************** */
class Navigator {
  login(req, res) {
    let userFromDatabase = req.result;
    // let usercheck = db.checkUserWithUsernameAndPassword(user);
    if(userFromDatabase){
    let user= {name:userFromDatabase.user_name}
      if (user) {
        // create token by sign: +> take our payload and serialize user object
        // in order to seriallize this user object, we need a secret key (in env)
        const accessToken = generateAccessToken(user)

        const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET)

        // push refresh token to the storage 
        refreshTokens.push(refreshToken)

        // return the JWT token for the future API calls
        res.json({
          // requestData : req.user,
          status:200,
          success: true,
          message: 'Authentication successful!',
          accessToken: accessToken,
          refreshToken: refreshToken
        });
      } else {
        res.sendStatus(403).json({
          success: false,
          message: 'Incorrect username or password'
        });
      }
    }else{
      res.sendStatus(500)
    }
    
  }
  index(req, res) {
    res.json({
      success: true,
      message: 'Index page'
    });
  }
  
}
/*************************** check token  ***************************************************** */

function authenticateToken(req, res, next) {
  const authHeader = req.headers['authorization']
  //if we have authheader then return authHeader authen token else : undifined
  const token = authHeader && authHeader.split(' ')[1]

  if (token == null) return res.sendStatus(401) // unauthorized

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    // if (err) return res.sendStatus(403) // token is not valid, you dint have permission 
    if (err) return res.send(403).json({
      success: false,
      message: 'Authentication failed! you dint have permission'
    }); // token is not valid, you dint have permission 
    req.user = user // you have permission, attach user info, to view .
    next()
  })
}

function generateAccessToken(user) {
  return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: '5mins'
  }) // 30mims 
}

function main() {
  /*************************** API CONFIG  ***************************************************** */
  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization,responseType');
    next();
  });

  // dung parse json 
  // dung form ?? 
  app.use(
    bodyParser.urlencoded({
      extended: true
    })
  )
  app.use(bodyParser.json())
  let navigator = new Navigator(); 
  
   
  /*************************** endpoints ***************************************************** */
  // app.get('/', db.getStories)
  app.get('/stories',authenticateToken, db.getStories)
  app.get('/my-stories/:author',authenticateToken, db.getMyStories)
  app.get('/stories/:id',authenticateToken, db.getStoryById)
  app.post('/stories',authenticateToken, db.createStory) 
  app.put('/stories/:id',authenticateToken, db.updateStory)
  app.put('/stories-full/:id',authenticateToken, db.updateFullStory) 
  app.delete('/stories/:id',authenticateToken, db.deleteStory)

  app.get('/users',authenticateToken, db.getUsers)
  app.get('/users/:id',authenticateToken, db.getUserById)
  app.get('/users-by-name/:name',authenticateToken, db.getUserByName)
  app.get('/header',authenticateToken, db.getInfoByHeader)
  app.post('/users', db.createUser)
  app.put('/users/:id',authenticateToken, db.updateUser)
  app.delete('/users/:id',authenticateToken, db.deleteUser)

  // ----------------------------------------
  app.post('/login', db.checkUserWithUsernameAndPassword ,navigator.login);

  app.get('/', authenticateToken, navigator.index);

  app.post('/get-another-token', (req, res) => {
    const refreshToken = req.body.token
    if (refreshToken == null) return res.sendStatus(401)
    if (!refreshTokens.includes(refreshToken)) return res.sendStatus(403)
  // user o day duoc lay tu refresh token
    jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
      if (err) return res.sendStatus(403)
      const accessToken = generateAccessToken({
        name: user.name
      })
      res.json({
        accessToken: accessToken
      })
    })
  })

  const posts = [{user_name: 'admin',title: 'post 1'},{user_name: 'duc',title: 'post 1'}, { user_name: 'ngan',title: 'post 2'}]

  app.get('/getposts', authenticateToken, (req, res) => {
    res.json(posts.filter(p => p.user_name === req.user.name))
  })
  const port = process.env.PORT || 3000;
  app.listen(3000, () => console.log(`Server running on port ${port}...`))
}
main();