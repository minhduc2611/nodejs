--https://rextester.com/l/postgresql_online_compiler

CREATE DATABASE minhduc_database
OWNER =  minhduc;
--CREATE USER minhduc WITH ENCRYPTED PASSWORD 'duc123321';
-- ALTER DATABASE minhduc_database OWNER TO minhduc;
-- \password role;  to set password
-- \c dbname;  to connect
CREATE TABLE story (
   story_id INT PRIMARY KEY,
   story_title VARCHAR (50) UNIQUE NOT NULL,
   story_url VARCHAR (100) NOT NULL,
   story_content VARCHAR (355) NOT NULL,
   story_created_date DATE NOT NULL
); 

INSERT INTO story (story_id, story_title, story_url,story_content,story_created_date)
VALUES
   (1, 'A Date in valentine s day','story url 1' , ' My Friend : Do you have a date for Valentine’s Day? Me: Yes, it is February 14th.','1-2-2020' ),
   (2, 'I’LL TAKE SOMETHING ELSE','story url 2' , ' My 35-year-old son and I had just finished our meal when I realized I’d left my wallet in my truck. As I headed out the door, I told the waitress what had happened. “But don’t worry,” I said with a grin. “I’m leaving my son for collateral.” She looked at him. He winked at her. She turned back to me. “What else you got?”','1-2-2020' ),
   (3, 'story title3','story url 3' , ' story content 3','1-2-2020' ),
   (4, 'story title4','story url 4' , ' story content 4','1-2-2020' ),
   (5, 'story title5','story url 5' , ' story content 5','1-2-2020' ),
   (6, 'story title6','story url 6' , ' story content 6','1-2-2020' ),
   (7, 'story title7','story url 7' , ' story content 7','1-2-2020' ),
   (8, 'story title8','story url 8' , ' story content 8','1-2-2020' ) ;

   
   ALTER TABLE story 
   ADD COLUMN story_image_url VARCHAR,
   ADD COLUMN story_author VARCHAR,
   ADD COLUMN story_likes INT,
   ADD COLUMN story_comment JSON;
   ALTER TABLE story 
   ADD COLUMN story_author_image VARCHAR;

   ALTER TABLE story
   ALTER COLUMN story_likes TYPE INT USING story_likes::integer;

   ALTER TABLE story
   ALTER COLUMN story_comment TYPE JSON USING story_comment::json; 

   UPDATE story SET story_comment = array_append(story_comment,'new item') WHERE story_id =1;

   INSERT INTO story(
       story_id, 
       story_title, 
       story_url, 
       story_content, 
       story_created_date,
       story_image_url,
       story_author,
       story_likes,
       story_comment ) VALUES (
           9,
           ' alentinms day', 
           'story url 8', 
           'kjbjbk', 
           '1-2-2020',
           'asa',
           'asa',
           3,
           '{"sdas","abc",1,2}'),
            (
           10,
           ' alentinms daasdy', 
           'story url 8', 
           'kjbjbk', 
           '1-2-2020',
           'asa',
           'asa',
           3,
           ARRAY [ '(408)-743-9045','(408)-567-7834' ]);

   SELECT * FROM story ;


CREATE TABLE users (
   user_id SERIAL PRIMARY KEY,
   user_name VARCHAR (50) NOT NULL,
   user_email VARCHAR (100) NOT NULL,
   user_password VARCHAR (50) NOT NULL,
   user_profile_image_url VARCHAR(50),
   user_role VARCHAR(50),
   user_created_date TIMESTAMPTZ DEFAULT Now()
);


INSERT INTO users (user_name,user_email,user_password,user_profile_image_url,user_role)

VALUES
   ('minh duc','minhduc@gmail.com' , '123','ds','ds'),
   ('hanh ngan','hanhngan@gmail.com' , '123','ds','ds');




--    [
--     {
--         "story_id": 4,
--         "story_title": "A Date in valentine's day",
--         "story_url": "story url 4",
--         "story_content": "My Friend : Do you have a date for Valentine’s Day? Me: Yes, it is February 14th.",
--         "story_created_date": "2019-12-27T17:00:00.000Z",
--         "story_image_url": "cat-fly.jpg",
--         "story_author": "minhduc",
--         "story_likes": 123,
--         "story_comment": {
--             "minhduc": "this picture is so beautiful 😄, where did you get this ?",
--             "hanhngan": "wow, interesting"
--         },
--         "story_author_image": "minhduc.jpg"
--     },
--     {
--         "story_id": 5,
--         "story_title": "i got a dog joke for you ",
--         "story_url": "story url 5",
--         "story_content": "Q: What do you call a large dog that meditates?\nA: Aware wolf.",
--         "story_created_date": "2019-12-28T17:00:00.000Z",
--         "story_image_url": "dog.jpg",
--         "story_author": "hanhngan",
--         "story_likes": 422,
--         "story_comment": {
--             "minhduc": "ha ha ha 😄, this dog is hillarious",
--             "hanhngan": "hi every one"
--         },
--         "story_author_image": "hanhngan.jpg"
--     },
--     {
--         "story_id": 6,
--         "story_title": "Is this even a bird ? ",
--         "story_url": "story url 6",
--         "story_content": "I took this pis yesterday, have a look at this bird",
--         "story_created_date": "2019-12-28T17:00:00.000Z",
--         "story_image_url": "owe.jpg",
--         "story_author": "minhduc",
--         "story_likes": 12,
--         "story_comment": {
--             "hanhngan": "i hate this"
--         },
--         "story_author_image": "minhduc.jpg"
--     },
--     {
--         "story_id": 7,
--         "story_title": "my cat",
--         "story_url": "My cat",
--         "story_content": "This is the cutest cat ive ever seen 😱",
--         "story_created_date": "2019-12-29T17:00:00.000Z",
--         "story_image_url": "cat.jpg",
--         "story_author": "minhduc",
--         "story_likes": 512,
--         "story_comment": {
--             "hanhngan": "I saw this cat and like it right away"
--         },
--         "story_author_image": "minhduc.jpg"
--     },
--     {
--         "story_id": 8,
--         "story_title": "Live",
--         "story_url": "story url 8",
--         "story_content": "Live now",
--         "story_created_date": "2019-12-29T17:00:00.000Z",
--         "story_image_url": "buffalo.jpg",
--         "story_author": "hanhngan",
--         "story_likes": 114,
--         "story_comment": {
--             "anh duy": "you are a phylosopher right ? "
--         },
--         "story_author_image": "hanhngan.jpg"
--     },
--     {
--         "story_id": 9,
--         "story_title": "a very happy day",
--         "story_url": "story url 8",
--         "story_content": "ive just buy a dogggg",
--         "story_created_date": "2019-12-27T17:00:00.000Z",
--         "story_image_url": "golden.jpg",
--         "story_author": "thuydung",
--         "story_likes": 3,
--         "story_comment": {
--             "minhduc": "wow, so cuteee"
--         },
--         "story_author_image": "thuydung.jpg"
--     },
--     {
--         "story_id": 10,
--         "story_title": "ive just bought a parrot",
--         "story_url": "story url ",
--         "story_content": " take a look guys ",
--         "story_created_date": "2019-12-29T17:00:00.000Z",
--         "story_image_url": "parrot.jpg",
--         "story_author": "hanh ngan",
--         "story_likes": 114,
--         "story_comment": {
--             "minhduc": "wowww ",
--             "hanhngan": "where did you bought this ",
--             "thuydung": "the pet store near your house "
--         },
--         "story_author_image": "thuydung.jpg"
--     },
--     {
--         "story_id": 88,
--         "story_title": "alentinms day",
--         "story_url": "story url 8",
--         "story_content": "kjbjbk",
--         "story_created_date": "2019-12-28T17:00:00.000Z",
--         "story_image_url": "lying-grass.jpg",
--         "story_author": "thuydung",
--         "story_likes": 3,
--         "story_comment": null,
--         "story_author_image": "thuydung.jpg"
--     },
--     {
--         "story_id": 100,
--         "story_title": "live happily",
--         "story_url": "story url ",
--         "story_content": "beautiful now  ~ ",
--         "story_created_date": "2019-12-29T17:00:00.000Z",
--         "story_image_url": "buffalo.jpg",
--         "story_author": "hanh ngan",
--         "story_likes": 114,
--         "story_comment": {
--             "minhduc": "you are a phylosopher right ? "
--         },
--         "story_author_image": "hanhngan.jpg"
--     }
-- ]