import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { UsersService } from 'src/app/services/users-service/users.service';
import { Component } from '@angular/core';
import { MessageService } from './services/messages-service/message.service';
import { formatDate } from '@angular/common';
import { UserLoginService } from './services/user-login-service/user-login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(
    private MessageService: MessageService,
    private userService: UsersService,
    private userLoginService : UserLoginService,
    private router : Router
    ) {
    this.getInfo()
  }
  title = 'duclm24-client';

  // co dang nhap chua ?
  haveSession = (localStorage.length == 1)? false : true 
  name = null ;
  expireTime = null;

  getInfo(){  
    this.userService.fetchUserSessionInfo().subscribe((data) =>  {
      this.name = 'Welcome, '+data['name']
      
      // this.expireTime =  formatDate(new Date(new Date - parseInt(data['exp']) * 1000))
      const format = 'dd/MM/yyyy, hh:mm';
      const locale = 'en-US';
      this.expireTime = formatDate(new Date(parseInt(data['exp']) * 1000), format, locale);


      localStorage.setItem("currentUserExp", String(data['exp']*1000)); // hard code



      // CHECK SESSION TIME OUT
      let timeLeft = ( data['exp']*1000 - Date.now())/1000; // sec 

      if(timeLeft>0){
        console.log("Time left : "+timeLeft)
        setTimeout(() => {
          this.log("your session is over, please try to login again ")
          // navigate to hoome page 
          setTimeout(() => {
            this.router.navigate(['/userlogin']).then(() => {
              window.location.reload();
            });
          }, 3*1000);
            
          console.log(timeLeft/60)
        }, timeLeft*1000);
      }else{
        console.log('time remain : '+timeLeft)
        this.router.navigate(['/userlogin'])
      }
     
      
    });
  }
  
  private log(message: string) {
    this.MessageService.add(`User Login Service: ${message}`);
  }
}
