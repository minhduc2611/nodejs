import { User } from './../../objects/user';
import { MessageService } from './../messages-service/message.service';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Story } from '../../objects/story';
import { catchError, map, tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class StoryServiceService {

  // this is for parsing json ? answer this quertion later
  // httpOptions = {
  //   headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  //   responseType: new StringConstructor
  // };
  httpOptions = {
    headers: new HttpHeaders(
      { 'content-type': 'application/json' ,
        'Authorization':'Bearer'+' '+localStorage.getItem("AT")
    }),

      // responseType: 'text'
  }

  constructor(
    private MessageService: MessageService,
    private http: HttpClient) { }
  private domainName = 'http://localhost:3000';
  private storiesUrl = 'http://localhost:3000/stories';

  /** GET story by id. Will 404 if id not found */
  fetchStories(): Observable<Story[]> {
    return this.http.get<Story[]>(this.storiesUrl,this.httpOptions).pipe(
      // tap(_ => this.action_log('fetched stories')),
      catchError(this.handleError<Story[]>('get stories', []))
    );
  }

  /** GET story by id. Will 404 if id not found */
  fetchMyStories(author:string): Observable<Story[]> {
    let authorName = author
    // '/my-stories/:author'
    let url = `${this.domainName}/my-stories/${authorName}`;
    return this.http.get<Story[]>(url,this.httpOptions).pipe(
      // tap(_ => this.action_log('fetched stories')),
      catchError(this.handleError<Story[]>('get my stories', []))
    );
  }

  /** GET story by id. Will 404 if id not found */
  fetchOneStoryById(id): Observable<Story> {
    const url = `${this.storiesUrl}/${id}`;
    return this.http.get<Story>(url,this.httpOptions).pipe(
      // tap(_ => this.action_log(`fetched story id=${id}`)),
      catchError(this.handleError<Story>(`getStory id = ${id}`))
    );
  }

  /** PUT: update the story on the server */
  updateStory(story: Story): Observable<any> {
    const url = `${this.storiesUrl}/${story.story_id}`;
    console.log(url);
    return this.http.put(url, story, this.httpOptions).pipe(
      tap(_ => this.action_log(`updated story id=${story.story_id}`)),
      catchError(this.handleError<any>('updateStory')) 
    );
  }
  /** DELETE: delete the story from the server */
  deleteStory(story: Story) {
    console.log('hi')
    const url = `${this.storiesUrl}/${story.story_id}`;
    console.log(url)
    console.log(story)
    return this.http.delete(url).pipe(
      tap(_ => this.action_log(`deleted story id=${story.story_id}`)),
      catchError(this.handleError<Story>('deleteStory'))
    );
    
  }

  /** Log a StoryService message with the MessageService */
  private action_log(message: string) {
    
    this.MessageService.add(`Story Service: ${message}`);
  }

  private defect_log(message: string) {
    this.MessageService.add(`Story Service: ${message}`);
  }


  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      let message =""
      if (error.status==403) {
        message = "you are forbidded from using this feature" }
        else if(error.status==401){
          message = "you have to log in OR you just dont have permission to do this action" 
        }
      // TODO: better job of transforming error for user consumption
      this.defect_log(`${operation} failed: ${message}`);

      // Let the app keep running by returning error info
      return of(result as T);
    };
  }
}