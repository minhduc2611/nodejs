import { Injectable } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';
@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(
    private _snackBar: MatSnackBar,
    ) { }
    
  messages: string[] = [];

  add(message: string) {
    this.messages.push(message);
    this.openSnackBar(message, "I got it")
  }

  clear() {
    this.messages = [];
  }
  deletemMess(index){
    // console.log( "deleted index number " + index, this.messages)
    this.messages.splice(index,1)
  }
  deletemFirstMess(){
    // console.log( "deleted index number " + index, this.messages)
    this.messages.shift()
  }

  // use mat snack bar

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 10000,
      verticalPosition: 'top'
    });
  }

}
