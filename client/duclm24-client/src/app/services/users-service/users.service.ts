import { MessageService } from './../messages-service/message.service';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { User } from 'src/app/objects/user';
@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private MessageService: MessageService,
    private http: HttpClient
  ) { }

  httpOptions = {
    headers: new HttpHeaders(
      { 'content-type': 'application/json','Authorization':'Bearer'+' '+localStorage.getItem("AT") }),
  }

  private usersUrl = 'http://localhost:3000/users';

  fetchUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl,this.httpOptions).pipe(
      // tap(_ => this.action_log('fetched users')),
      catchError(this.handleError<User[]>('getUsers', []))
    );
  }
  fetchUserSessionInfo() {
    const url = `http://localhost:3000/header`;
    return this.http.get(url,this.httpOptions).pipe(
      // tap(_ => this.action_log('fetched users')),
      // catchError(this.handleError('fetchUserSessionInfo'))
    );
  }
  fetchUserByName(name): Observable<User>  {
    const url = `http://localhost:3000/users-by-name/${name}`;
    return this.http.get<User>(url,this.httpOptions).pipe(
      // tap(_ => this.action_log('fetched users')),
      catchError(this.handleError<User>('fetchUserByName '))
    );
  }

  /** POST: add the user to the server */
  addUser(user: User): Observable<any> {
    return this.http.post<any>(this.usersUrl, user).pipe(
      tap(_ => this.action_log(`registered user name=${user.user_name}`)),
      catchError(this.handleError<any>('updateuser'))
    );
  }
  
  /** PUT: update the user on the server */
  updateUser(user: User): Observable<any> {
    const url = `${this.usersUrl}/${user.user_id}`;
    // console.log(url);
    return this.http.put(url, user,this.httpOptions).pipe(
      tap(_ => this.action_log(`updated user id=${user.user_id}`)),
      catchError(this.handleError<any>('updateuser'))
    );
  }

  deleteUser(user: User) {
    const url = `${this.usersUrl}/${user.user_id}`;
    return this.http.delete(url, this.httpOptions).pipe(
      tap(_ => this.action_log(`deleted user id=${user.user_id}`)),
      catchError(this.handleError<User>('deleteUser'))
    );
  }

  /** Log a UserService message with the MessageService */
  private action_log(message: string) {
    
    this.MessageService.add(`User Service: ${message}`);
  }

  private defect_log(message: string) {
    this.MessageService.add(`User Service: ${message}`);
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      let message = ""
      if (error.status==403) {
        message = "you are forbidded from using this feature" }
        else if(error.status==401){
          message = "you have to log in OR you just dont have permission on this action" 
        }
      // TODO: better job of transforming error for user consumption
      this.defect_log(`${operation} failed: ${message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
