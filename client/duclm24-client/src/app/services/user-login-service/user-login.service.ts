import { Router } from '@angular/router';
import { tap, catchError, map } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { User } from './../../objects/user';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MessageService } from '../messages-service/message.service';
import { error } from '@angular/compiler/src/util';
import { UsersService } from '../users-service/users.service';
    // RxJS v6+
    import { timer } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserLoginService {

  constructor(
    private MessageService: MessageService,
    private http: HttpClient,
    private router: Router,
    private userService:UsersService
  ) { }
  currentUser;
  private usersUrl = 'http://localhost:3000/login';
  /** POST: add the user to the server */
  login(user: User): Observable<any> {
    let loginUser = {
      "username": user.user_name,
      "password": user.user_password
    }
    // console.log(loginUser)

    return this.http.post(this.usersUrl, loginUser)
      .pipe(
          map((response: Response) => {
            console.log(response)
            if (typeof(Storage) !== "undefined") {
              // Store
              // this.currentUser = response
              localStorage.setItem("AT", response['accessToken']); // hard code
              localStorage.setItem("RT", response['refreshToken']); // hard code
              localStorage.setItem("currentUserExp", response['exp']); // hard code
              // // Retrieve
              // localStorage.getItem("AT");
            }
            // console.log(localStorage.getItem("AT"))
            return response
          }),
          tap(_ => this.log(`login as user name:${user.user_name}, you have 5 min session`)),
          catchError(error => {return [error]})
        );
  }
  getAnotherToken(){
    let usersUrl = 'http://localhost:3000/get-another-token';
    return this.http.post(this.usersUrl, localStorage.getItem("RT"))
      .pipe(
          map((response: Response) => {
            console.log(response)
            if (typeof(Storage) !== "undefined") {
              // Store
              // this.currentUser = response
              localStorage.setItem("AT", response['accessToken']); // hard code
              // localStorage.setItem("RT", response['refreshToken']); // hard code
              // localStorage.setItem("currentUserExp", response['exp']); // hard code
              // // Retrieve
              // localStorage.getItem("AT");
            }
            // console.log(localStorage.getItem("AT"))
            return response
          }),
          // tap(_ => this.log(`login as user name=${user.user_name}`)),
          catchError(error => {return [error]})
        );
    
  }

  isLogin(){


    // // let expTime = parseInt(localStorage.getItem("currentUserExp"))/1000/60
    let timeLeft = (parseInt(localStorage.getItem("currentUserExp")) - Date.now())/1000; // sec 
    // console.log(timeLeft)
    // console.log((timeLeft > 0  ) ? true : false)
    // // console.log(60*1000)

    if(timeLeft<0){
      // console.log(timeLeft)
      // setTimeout(() => {
      //   this.log("your session is over, please try to login again ")
      //   // navigate to hoome page 
      //   this.router.navigate(['/userlogin']).then(() => {
      //     window.location.reload();
      //   });
      //   console.log(timeLeft)
      // }, timeLeft*1000);
    // }else{
      console.log(timeLeft)

      this.router.navigate(['/userlogin'])
    }
    if(localStorage.getItem("AT")){
      // if(timeLeft > 0){
      //   return true;
      // }else{
      //   this.getAnotherToken()
      //   this.log("Your session is out of time please login again")
  
      //   // setTimeout(() => {
      //     // navigate to login page 
      //     // this.router.navigate(['/userlogin'])
  
      //   // }, 60*1000);
  
      // }
    }else{
      this.log("you haven't login ")
      this.router.navigate(['/userlogin'])
    }

    
  } 

  /** Log a UserService message with the MessageService */
  private log(message: string) {
    this.MessageService.add(`User Login Service: ${message}`);
  }
  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return error['status'];
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      let message =""
      if (error.status==403) {
        message = "you are forbidded " }
        else if(error.status==401){
          message = "you have to log in OR you just dont have permission to do this action" 
        }else if(error.status==500){
          message = "wrong username or password" 
        }
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${message}`)

      // Let the app keep running by returning an empty result.
      return of(result as T);
      // return error.;
    };
  }
}
