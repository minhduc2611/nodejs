export interface Story {
    story_id: number;
    story_title: string;
    story_url: string;
    story_content: string;
    story_created_date: string;
    story_image_url: string;
    story_author: string;
    story_comment: string;
    story_likes: number;
    story_author_image: string;

  }