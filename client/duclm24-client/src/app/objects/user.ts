export interface User {
    user_id: number;
    user_name: string;
    user_email: string;
    user_password: string;
    user_created_date: string;
    user_profile_image_url: string;
    user_role: string;

  }