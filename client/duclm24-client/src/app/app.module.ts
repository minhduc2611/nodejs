import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { StoryManagerComponent } from './components/story-manager/story-manager.component';
import {HttpClientModule} from '@angular/common/http';
import { StoryServiceService } from './services/story-service/story-service.service';
import { UserManagerComponent } from './components/user-manager/user-manager.component';
import { StorySingleComponent } from './components/story-single/story-single.component';
import { MessagesComponent } from './components/messages/messages.component';
import { ModalModule } from 'ngx-bootstrap/modal/';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/** ng add @angular/material */
import { MatSliderModule } from '@angular/material/slider';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {MatIconModule} from '@angular/material/icon';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatCardModule} from '@angular/material/card';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatBadgeModule} from '@angular/material/badge';

import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';
import { UserRegisterComponent } from './components/user-register/user-register.component';
import { UserLoginComponent } from './components/user-login/user-login.component';
import { MyStoryManagerComponent } from './components/my-story-manager/my-story-manager.component';
const appRoutes: Routes = [
  { path: 'storymanager', component: StoryManagerComponent },
  { path: 'mystorymanager', component: MyStoryManagerComponent },
  { path: 'usermanager', component: UserManagerComponent },
  { path: '', redirectTo: '/storymanager', pathMatch: 'full' },  
  { path: 'userregister', component: UserRegisterComponent },
  { path: 'userlogin', component: UserLoginComponent },
  { path: 'storysingle/:id', component: StorySingleComponent },
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    StoryManagerComponent,
    UserManagerComponent,
    StorySingleComponent,
    MessagesComponent,
    UserRegisterComponent,
    UserLoginComponent,
    MyStoryManagerComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,

    RouterModule.forRoot(
      appRoutes,
      // { enableTracing: true } // <-- debugging purposes only
      ),
    ModalModule.forRoot(),
    FormsModule,
    BrowserAnimationsModule,

    // material modules
    MatSliderModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatIconModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatCardModule,
    MatTooltipModule,
    MatMenuModule,
    MatProgressBarModule,
    MatBadgeModule,
  ],
  providers: [StoryServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
