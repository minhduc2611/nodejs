import { Story } from './../../objects/story';
import { Component, OnInit } from '@angular/core';
import {StoryServiceService} from '../../services/story-service/story-service.service'
import { UserLoginService } from 'src/app/services/user-login-service/user-login.service';
@Component({
  selector: 'app-story-manager',
  templateUrl: './story-manager.component.html',
  styleUrls: ['./story-manager.component.css']
})
export class StoryManagerComponent implements OnInit {
  stories$=[];
  
  listToggle : boolean = false;
  selectedStory;
  constructor(
    private storyService: StoryServiceService,
    private userLoginService: UserLoginService,
    ) {
    this.userLoginService.isLogin()
   }

  ngOnInit(): void {
    this.fetchStories()
    
  }

  addLikes(id){

  }
  

  fetchStories(){
    this.storyService.fetchStories()
        .subscribe(
          stories => {
          this.stories$ = stories
        },
        error => { console.log(' fetch fail ' +error); // Error if any
        }
        )
  }
  passObject(story : Story){
    console.log(story)
    this.selectedStory = story
  }
 
}
