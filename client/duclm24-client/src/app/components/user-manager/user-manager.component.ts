import { UserLoginService } from './../../services/user-login-service/user-login.service';
import { logging } from 'protractor';
import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { UsersService } from 'src/app/services/users-service/users.service';
import { User } from 'src/app/objects/user';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-user-manager',
  templateUrl: './user-manager.component.html',
  styleUrls: ['./user-manager.component.css']
})
export class UserManagerComponent implements OnInit {

  constructor(
    private usersService: UsersService,
    private modalService: BsModalService,
    private userLoginService : UserLoginService
  ) {
    this.userLoginService.isLogin()
   }

  modalRef: BsModalRef;
  
  /* Open Model
  npm install ngx-bootstrap --save

  * reference: https://valor-software.com/ngx-bootstrap/#/modals
  */
 openModalWithClass(template: TemplateRef<any>) {
  this.modalRef = this.modalService.show(
    template,
    Object.assign({}, { class: 'gray modal-lg' })
  );
}
  ngOnInit(): void {
    this.fetchUsers()
  }

  @Input() user: User;

  users$ : User[] = [];
  
  activeEntityID;

  toggle(user) {
    this.activeEntityID = (this.activeEntityID == user) ? null : user;
  }

  fetchUsers() {
    this.usersService.fetchUsers()
      .subscribe(users => this.users$ = users)
  }

  save(user): void {
    console.log(user)
    // this.activeEntityID = null 
    this.usersService.updateUser(user)
      .subscribe(() => this.activeEntityID = null);
  }

  delete(user): void {
    if(confirm("delete ? ")){
      this.usersService.deleteUser(user)
      .subscribe(() => {
        this.activeEntityID = null;
        let indx = this.users$.findIndex(i => i.user_id == user.user_id) 
        this.users$.splice(indx,1);
      });
    }
  }


  add(user){
    // this.usersService.addUser()
    //   .subscribe(() => this.activeEntityID = null);
  }

}
