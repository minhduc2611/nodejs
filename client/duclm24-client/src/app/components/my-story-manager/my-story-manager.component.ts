import { User } from './../../objects/user';
import { Story } from './../../objects/story';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { StoryServiceService } from 'src/app/services/story-service/story-service.service';
import { UserLoginService } from 'src/app/services/user-login-service/user-login.service';
import { UsersService } from 'src/app/services/users-service/users.service';
import { type } from 'os';

@Component({
  selector: 'app-my-story-manager',
  templateUrl: './my-story-manager.component.html',
  styleUrls: ['./my-story-manager.component.css']
})
export class MyStoryManagerComponent implements OnInit {
  stories$ = [];
  currentUserName$: string;
  modalRef: BsModalRef;
  listToggle: boolean = false;
  selectedStory;
  constructor(private storyService: StoryServiceService,
    private modalService: BsModalService,
    private userLoginService: UserLoginService,
    private userService: UsersService
  ) {
    this.userLoginService.isLogin()

  }

  ngOnInit(): void {
    this.getCurrentUser()
    this.fetchStories()

  }

  /* Open Model
  npm install ngx-bootstrap --save

  * reference: https://valor-software.com/ngx-bootstrap/#/modals
  */
  openModalWithClass(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'gray modal-lg' })
    );
  }
  passObject(story: Story) {
    // console.log(story)
    this.selectedStory = story
    // console.log(this.selectedStory)
  }

  addLikes(id) {

  }

  save() { }


  hideTemplate(result: boolean) {
    if (result)
      this.modalRef.hide()
  }



  getCurrentUser(): string {
    this.userService.fetchUserSessionInfo().subscribe(data => {
      this.currentUserName$ = data['name']

      console.log(data)
      console.log(this.currentUserName$)
      return this.currentUserName$
    },
      error => {
        console.log(' fetch fail ' + error); // Error if any
      })
    // this.userService.fetchUserByName(this.currentUserName.user_name).subscribe(data=> {
    //   this.currentUserName = data
    // console.log(data)

    // })
    return this.currentUserName$
  }
  fetchStories() {
    let name
    this.userService.fetchUserSessionInfo().subscribe(data => {
      name = data['name']
      this.storyService.fetchMyStories(name)
        .subscribe(
          stories => {
            console.log(stories)
            this.stories$ = stories
          },
          error => {
            console.log(' fetch fail ' + error); // Error if any
          }
        )
      // console.log(data)
      // console.log(this.currentUserName$)
      // return this.currentUserName$
    })
  }
  delete() {
    if (confirm('delete ? ')) {
      // delete the story in the array first

      let indx = this.stories$.findIndex(i => i.story_id == this.selectedStory.story_id)
      this.stories$.splice(indx, 1);
      console.log(indx)

      //delete it in the database
      // console.log(this.selectedStory)
      this.storyService.deleteStory(this.selectedStory)

    } else {
      return false;
    }
  }
}
