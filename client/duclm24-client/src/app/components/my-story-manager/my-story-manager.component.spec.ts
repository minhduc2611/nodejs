import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyStoryManagerComponent } from './my-story-manager.component';

describe('MyStoryManagerComponent', () => {
  let component: MyStoryManagerComponent;
  let fixture: ComponentFixture<MyStoryManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyStoryManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyStoryManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
