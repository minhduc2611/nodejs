import { map, tap, catchError } from 'rxjs/operators';
import { MessageService } from './../../services/messages-service/message.service';
import { UserLoginService } from './../../services/user-login-service/user-login.service';
import { User } from 'src/app/objects/user';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {

  constructor(
    private loginService: UserLoginService,
    private messageService: MessageService,
    private router: Router,
  ) {
    this.clearSession()
  }

  ngOnInit(): void {
  }

  newUser: User = {
    "user_id": null,
    "user_name": null,
    "user_email": null,
    "user_password": null,
    "user_created_date": null,
    "user_profile_image_url": null,
    "user_role": null,
  }
  activeProgressBar= false;

  onSubmit() {
    // console.log(this.newUser)
    this.loginService.login(this.newUser)
      .subscribe(
        (response: Response) => {
          // console.log(typeof(response['status'])) // Data which is returned by call
          if (response['status'] == 200) {
            this.activeProgressBar= true;
            setTimeout(() => {
              // navigate to hoome page 
              this.router.navigate(['/storymanager']).then(() => {
                window.location.reload();
              });

            }, 3000);

          } else if (response['status'] == 500) {
            // mess 
            this.log(`wrong user name or password`)
          }
        },
        error => {
          console.log(error); // Error if any
        }
      )
  }

  clearSession() {
    if (localStorage.length != 0) {
      localStorage.clear();
      window.location.reload();
      console.log('ok to reload')
    }
  }

  /** Log a UserService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`User Login : ${message}`);
  }
  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  // private handleError<T>(operation = 'operation', result?: T) {
  //   return (error: any): Observable<T> => {

  //     // TODO: send the error to remote logging infrastructure
  //     console.error(error); // log to console instead
  //     let message =""
  //     if (error.status==403) {
  //       message = "you are forbidded " }
  //       else if(error.status==401){
  //         message = "you have to log in OR you just dont have permission to do this action" 
  //       }else if(error.status==500){
  //         message = "wrong username or password" 
  //       }
  //     // TODO: better job of transforming error for user consumption
  //     this.log(`${operation} failed: ${message}`)

  //     // Let the app keep running by returning an empty result.
  //     return of(result as T);
  //     // return error.;
  //   };
  // }
}
