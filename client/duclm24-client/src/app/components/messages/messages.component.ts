
import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../services/messages-service/message.service';
import { Observable,interval, Subscription,timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';


@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  constructor(
    public messageService: MessageService,
    ) { }

  ngOnInit(): void {
    timer(0, 15000).subscribe(result => {
      if(this.messageService.messages.length > 0){
        this.messageService.deletemFirstMess()
    }})
  }



  

}
