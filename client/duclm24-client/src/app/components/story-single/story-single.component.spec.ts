import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StorySingleComponent } from './story-single.component';

describe('StorySingleComponent', () => {
  let component: StorySingleComponent;
  let fixture: ComponentFixture<StorySingleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StorySingleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StorySingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
