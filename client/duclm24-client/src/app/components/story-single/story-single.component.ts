import { Story } from './../../objects/story';
import { Component, OnInit, Input, Output, TemplateRef, EventEmitter } from '@angular/core';
import { StoryServiceService } from '../../services/story-service/story-service.service'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { UserLoginService } from 'src/app/services/user-login-service/user-login.service';
@Component({
  selector: 'app-story-single',
  templateUrl: './story-single.component.html',
  styleUrls: ['./story-single.component.css']
})
export class StorySingleComponent implements OnInit {
  
  modalRef: BsModalRef;

  constructor(
    private storyService : StoryServiceService,
    private route: ActivatedRoute,
    private location: Location,
    private modalService: BsModalService,
    private userLoginService: UserLoginService,
    ) {
    this.userLoginService.isLogin()
  }
  
  /* Open Model
  npm install ngx-bootstrap --save

  * reference: https://valor-software.com/ngx-bootstrap/#/modals
  // */
  // openModalWithClass(template: TemplateRef<any>) {
  //   this.modalRef = this.modalService.show(
  //     template,
  //     Object.assign({}, { class: 'gray modal-lg' })
  //   );
  // }
  
  @Input() storyInput : Story;
  story : Story;
  @Output() updatedStory : Story;
  @Output() close = new EventEmitter<boolean>();


  ngOnInit(): void {
    this.setStory();
    // this.getOneStory();
  }

  // getOneStory(): void{
  //   const id = +this.route.snapshot.paramMap.get('id');

  //   this.storyService.fetchOneStoryById(id).subscribe(s => {
  //     this.story = s[0]
  //   })
  // }
  setStory(){
    this.story = this.storyInput;
  }
  goBack(): void {
    this.location.back();
  }
  save(): void {
    this.storyService.updateStory(this.story)
      .subscribe(() => this.closeTemp());
  }
  closeTemp(){
    this.close.emit(true);
  }
  // delete(){
  //   if(confirm('delete ? ')){
  //     this.storyService.deleteStory(this.story)
  //     .subscribe(()=> this.goBack())
  //   }else{
  //     return false;
  //   }
  // }
}
