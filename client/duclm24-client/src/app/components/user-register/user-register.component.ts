import { Router } from '@angular/router';
import { User } from 'src/app/objects/user';
import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users-service/users.service';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {

  constructor(
    private userService : UsersService,
    private router : Router,
  ) { }

  ngOnInit(): void {
  }
  hide = true;

  newUser: User={
    "user_id":  null,
    "user_name":  null,
    "user_email":  null,
    "user_password": null,
    "user_created_date": null,
    "user_profile_image_url": null,
    "user_role":null,
  }

  addUser(): void {
    console.log(this.newUser)
    this.userService.addUser(this.newUser)
      .subscribe(() => {
        //come action 
        this.router.navigate(['/userlogin']).then(() => {
          window.location.reload();
        });
      }); 
  }

}
